<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 13/01/2016
 * Time: 22:21
 */
namespace ModernWays\Webshop\View;

class Shared
{
    public function Header()
    {
        include('Template/Shared/Header.php');
    }

    public function Footer()
    {
        include('Template/Shared/Footer.php');
    }
}
